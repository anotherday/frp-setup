#!/bin/bash

WEB_USER=${WEB_USER:-admin}
WEB_PASS=${WEB_PASS:-admin123}
WEB_PORT=${WEB_PORT:-19090}

cd $(dirname $(readlink -f "$0"))

[ ! -d ./log ]&& mkdir -p ./log

echo  "# webproc -p ${WEB_PORT} --user ${WEB_USER} --pass ${WEB_PASS}"

nohup ${PWD}/webproc -p ${WEB_PORT} --user "${WEB_USER}" --pass "${WEB_PASS}"  --configuration-file ./frpc.ini -- ./frpc  > log/webproc.log 2>&1 &
