#!/bin/bash
PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:~/bin
###export###
export PATH
export FRP_URL=https://gitee.com/anotherday/frp-setup/releases/download/v0.48.0-frp-amd64/frp_0.48.0_linux_amd64.tar.gz
export WEBPROC_URL=https://gitee.com/anotherday/frp-setup/releases/download/v0.4.0-webproc/webproc_0.4.0_linux_amd64.gz


# $1 dl_url ; $2 output_filename 
wget_files(){
    dl_url=$1
    out_file=$2
    # test url 
    wget -t 2 -q --spider ${dl_url}
    rst_code=$?
    if [ "x${rst_code}" != "x0" ];then
        echo -n -e "ERROR: The URL ${dl_url} is invalid, exit 1."
        exit 1
    fi
    wget -t 2 -q -c -O ${out_file} ${dl_url}
}


run_install_frps(){


}

run_install_frpc(){


}




# 选择安装软件（frps/frpc, 0.48.0-amd64）
# 1. frps
# 2. frpc

choice_install(){
    # select frps/frpc to install 
    choice_id="1"
    echo ""
    echo -n -e "Select the soft to install:"
    echo -n -e "  1. frps (server)"
    echo -n -e "  2. frpc (client)"
    read -e -p "(Default: ${choice_id}):" choice_id
    [ -z "${choice_id}" ] && choice_id="${choice_id}"
    # run install 
    if [ "x${choice_id}" = "x1" ];then
        echo ""
        echo -n -e "Start install frps ..."
        run_install_frps
    else
        echo ""
        echo -n -e "Start install frpc ..."
        run_install_frpc
    fi
}

